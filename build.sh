#!/usr/bin/env sh
"$1"                       \
    ./core/zeno.c          \
    frontend.c             \
    -I./core               \
    -g                     \
    -std=c89               \
    -Wall                  \
    -pedantic              \
    -Dzeno_defaults        \
    -o zeno -ldl -lncurses

cd ./modules;
./build.sh "$1";
cd ..;
