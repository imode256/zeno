#include <zeno.h>

zeno_bool alu(zeno_core *core,
              zeno_cell  self) {
    switch(core->memory[100]) {
        case 1: {
            core->memory[103] = core->memory[101] +  core->memory[102];
            break;
        }
        case 2: {
            core->memory[103] = core->memory[101] -  core->memory[102];
            break;
        }
        case 3: {
            core->memory[103] = core->memory[101] <  core->memory[102];
            break;
        }
        case 4: {
            core->memory[103] = core->memory[101] == core->memory[102];
            break;
        }
        case 5: {
            core->memory[103] = core->memory[101] >  core->memory[102];
            break;
        }
        case 6: {
            core->memory[103] = core->memory[101] && core->memory[102];
            break;
        }
        case 7: {
            core->memory[103] = core->memory[101] || core->memory[102];
            break;
        }
        case 8: {
            core->memory[103] = !core->memory[101];
            break;
        }
        default: break;
    }
    return zeno_true;
}

