#include <zeno.h>
#if defined(zeno_immediate_enable)
zeno_bool instruction(zeno_core  *core,
                      zeno_cell  self) {
    zeno_cell immediate;
    zeno_cell source;
    zeno_cell destination;
    if(core->memory[zeno_memory_size - 1] != 0) {
        core->active = zeno_false;
        return zeno_true;
    }
    core->memory[0] %= zeno_memory_size;
    immediate        = core->memory[core->memory[0]];
    source           = core->memory[(core->memory[0] + 1) % zeno_memory_size];
    destination      = core->memory[(core->memory[0] + 2) % zeno_memory_size];
    destination     %= zeno_memory_size;
    if(immediate == zeno_true) {
        core->memory[destination] = source;
    }
    else {
        source                    %= zeno_memory_size;
        core->memory[destination]  = core->memory[source];
    }
    if(destination != 0) {
        core->memory[0] += 3;
    }
    return zeno_true;
}
#else
zeno_bool instruction(zeno_core  *core,
                      zeno_cell  self) {
    zeno_cell source;
    zeno_cell destination;
    if(core->memory[zeno_memory_size - 1] != 0) {
        core->active = zeno_false;
        return zeno_true;
    }
    core->memory[0] %= zeno_memory_size;
    source           = core->memory[core->memory[0]];
    destination      = core->memory[core->memory[0] + 1];
    source          %= zeno_memory_size;
    destination     %= zeno_memory_size;
    core->memory[destination]  = core->memory[source];
    if(destination != 0) {
        core->memory[0] += 2;
    }
    return zeno_true;
}
#endif
