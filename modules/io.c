#include <ncurses.h>
#include <zeno.h>

zeno_bool io(zeno_core *core,
             zeno_cell  self) {
    zeno_bool *initialized;
    zeno_cell  x, y;
    initialized = (zeno_bool*)core->modules[self].data;
    if(*initialized == zeno_false) {
        initscr();
        cbreak();
        refresh();
        noecho();
        curs_set(0);
        *initialized = zeno_true;
        return zeno_true;
    }
    if(core->memory[zeno_memory_size - 1] != 0) {
        getmaxyx(stdscr, y, x);
        mvprintw(y - 1, 0, "Halted: %lu. Window: %ux%u.", sizeof(zeno_core), x, y);
        refresh();
        getch();
        endwin();
        core->active = zeno_false;
        return zeno_true;
    }
    if(core->memory[200] != 0) {
        switch(core->memory[201]) {
            case 1: {
                mvaddch(core->memory[203], core->memory[202], core->memory[204]);
                break;
            }
            case 2: {
                clear();
                break;
            }
            case 3: {
                core->memory[204] = getch();
                break;
            }
            default: break;
        }
        refresh();
        core->memory[200] = 0;
    }
    return zeno_true;
}
