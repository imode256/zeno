#!/usr/bin/env sh

"$1"                      \
    -fPIC                 \
    -c *.c ../core/zeno.c \
    -I../core             \
    -g                    \
    -std=c89              \
    -Wall                 \
    -pedantic             \
    -Dzeno_defaults

"$1" -shared *.o -o modules.so;

rm *.o;
