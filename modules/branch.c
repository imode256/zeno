#include <zeno.h>

zeno_bool branch(zeno_core *core,
                 zeno_cell  self) {
    if(core->memory[220] != 0) {
        switch(core->memory[221]) {
            case 1: {
                if(core->memory[222] == 0) {
                    core->memory[0] = core->memory[223];
                }
                break;
            }
            case 2: {
                if(core->memory[222] != 0) {
                    core->memory[0] = core->memory[223];
                }
                break;
            }
            default: break;
        }
        core->memory[220] = 0;
    }
    return zeno_true;
}
