#include <stdio.h>
#include <string.h>
#include <dlfcn.h>
#include <zeno.h>

zeno_bool core_step(zeno_core *core) {
    zeno_cell   iterator;
    for(iterator = 0;
        iterator < zeno_module_count;
        iterator++) {
        if(core->modules[iterator].step != zeno_null) {
            if(core->modules[iterator].step(core, iterator) == zeno_false) {
                return zeno_false;
            }
        }
    }
    return core->active;
}

void release_all_handles(zeno_core *core) {
    void      **handles;
    zeno_cell   iterator;
    handles = (void**)core->data;
    for(iterator = 0;
        iterator < zeno_module_count;
        iterator++) {
        if(handles[iterator] != zeno_null) {
            dlclose(handles[iterator]);
        }
    }
    return;
}

zeno_bool load_module_config(zeno_core *core,
                             char      *filename) {
    void       **handles;
    FILE       * file;
    zeno_bool (* step)(zeno_core*,
                      zeno_cell);
    char         library[256];
    char         symbol[256];
    zeno_cell    module_id;
    file = fopen(filename, "r");
    if(file == zeno_null) {
        return zeno_false;
    }
    memset(library, 0, 256);
    memset(symbol, 0, 256);
    handles = (void**)core->data;
    while(fscanf(file, " %u %256s %256s", &module_id, library, symbol) != EOF) {
        handles[module_id] = dlopen(library, RTLD_LAZY);
        if(handles[module_id] == zeno_null) {
            printf("Error: %s\n", dlerror());
            release_all_handles(core);
            fclose(file);
            return zeno_false;
        }
        step = (zeno_bool(*)(zeno_core*, zeno_cell))dlsym(handles[module_id], symbol);
        if(zeno_module_set(core, module_id, step) == zeno_false) {
            printf("Error: %s\n", dlerror());
            release_all_handles(core);
            fclose(file);
            return zeno_false;
        }
        if(*(zeno_cell*)(dlsym(handles[module_id], "zeno_memory_size"))  != zeno_memory_size  ||
           *(zeno_cell*)(dlsym(handles[module_id], "zeno_cell_size"))    != zeno_cell_size    ||
           *(zeno_cell*)(dlsym(handles[module_id], "zeno_module_count")) != zeno_module_count ||
           *(zeno_cell*)(dlsym(handles[module_id], "zeno_module_size"))  != zeno_module_size  ||
           *(zeno_cell*)(dlsym(handles[module_id], "zeno_core_size"))    != zeno_core_size) {
            release_all_handles(core);
            fclose(file);
            return zeno_false;
        }
    }
    fclose(file);
    return zeno_true;
}

zeno_bool load_executable(zeno_core *core,
                          char      *filename) {
    FILE      *file;
    char      *format;
    zeno_cell  address;
    zeno_cell  value;
    file = fopen(filename, "r");
    if(file == zeno_null) {
        return zeno_false;
    }
    switch(zeno_cell_size) {
        case 1: {
            format = " %hhu";
            break;
        }
        case 2: {
            format = " %hu";
            break;
        }
        case 4: {
            format = " %u";
            break;
        }
        case 8: {
            format = " %lu";
            break;
        }
        default: return zeno_false;
    }
    for(address = 0;
        address < zeno_memory_size;
        address++) {
        if(fscanf(file, format, &value) == EOF) {
            break;
        }
        core->memory[address] = value;
    }
    fclose(file);
    return zeno_true;
}

int main(int argc, char **argv) {
    zeno_core c;
    if(argc != 3) {
        printf("Usage: %s <module config> <executable>\n", argv[0]);
        return -1;
    }
    zeno_core_set(&c, core_step);
    if(load_module_config(&c, argv[1]) == zeno_false) {
        printf("Error: couldn't load module config %s.\n", argv[1]);
        return -1;
    }
    if(load_executable(&c, argv[2]) == zeno_false) {
        printf("Error: couldn't load executable %s.\n", argv[2]);
        return -1;
    }
    while(c.step(&c));
    release_all_handles(&c);
    return 0;
}
