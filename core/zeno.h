#if !defined(zeno_included)
#define zeno_included
#if !defined(zeno_cell_type)
    #if !defined(zeno_defaults)
        #error "No cell type defined."
    #else
        #define zeno_cell_type unsigned int
    #endif
#endif
#if !defined(zeno_byte_type)
    #if !defined(zeno_defaults)
        #error "No byte type defined."
    #else
        #define zeno_byte_type unsigned char
    #endif
#endif
#if !defined(zeno_bool_type)
    #if !defined(zeno_defaults)
        #error "No boolean type defined."
    #else
        #define zeno_bool_type unsigned char
    #endif
#endif
#if !defined(zeno_module_data_size)
    #if !defined(zeno_defaults)
        #error "No module data size defined."
    #else
        #define zeno_module_data_size 1024
    #endif
#endif
#if !defined(zeno_core_data_size)
    #if !defined(zeno_defaults)
        #error "No core data size defined."
    #else
        #define zeno_core_data_size 1024
    #endif
#endif
#if !defined(zeno_core_module_count)
    #if !defined(zeno_defaults)
        #error "No module count defined."
    #else
        #define zeno_core_module_count 32
    #endif
#endif
#if !defined(zeno_core_memory_size)
    #if !defined(zeno_defaults)
        #error "No memory size defined."
    #else
        #define zeno_core_memory_size 1024
    #endif
#endif
#if !defined(zeno_null)
    #if !defined(zeno_defaults)
        #error "No null value defined."
    #else
        #define zeno_null ((void*)0)
    #endif
#endif

typedef zeno_cell_type zeno_cell;
typedef zeno_byte_type zeno_byte;
typedef zeno_bool_type zeno_bool;

enum {
    zeno_false = 0,
    zeno_true  = !zeno_false
};

typedef struct zeno_module zeno_module;
typedef struct zeno_core   zeno_core;

struct zeno_module {
    zeno_byte   data [zeno_module_data_size];
    zeno_bool (*step)(zeno_core*,
                      zeno_cell);
};

struct zeno_core {
    zeno_module   modules[zeno_core_module_count];
    zeno_cell     memory [zeno_core_memory_size];
    zeno_byte     data   [zeno_core_data_size];
    zeno_bool   (*step)  (zeno_core*);
    zeno_bool     active;
};

extern zeno_cell zeno_cell_size;
extern zeno_cell zeno_module_size;
extern zeno_cell zeno_core_size;
extern zeno_cell zeno_module_count;
extern zeno_cell zeno_memory_size;

zeno_bool zeno_module_set(zeno_core  *core,
                          zeno_cell   module,
                          zeno_bool (*step)(zeno_core*,
                                            zeno_cell));
zeno_bool zeno_core_set(zeno_core  *core,
                        zeno_bool (*step)(zeno_core*));
#endif
