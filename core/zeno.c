#include <zeno.h>

zeno_cell zeno_cell_size    = sizeof(zeno_cell);
zeno_cell zeno_module_size  = zeno_module_data_size;
zeno_cell zeno_core_size    = zeno_core_data_size;
zeno_cell zeno_module_count = zeno_core_module_count;
zeno_cell zeno_memory_size  = zeno_core_memory_size;

zeno_bool zeno_module_set(zeno_core  *core,
                          zeno_cell   module,
                          zeno_bool (*step)(zeno_core*,
                                            zeno_cell)) {
    zeno_cell iterator;
    if(core == zeno_null) {
        return zeno_false;
    }
    for(iterator = 0;
        iterator < zeno_module_data_size;
        iterator++) {
        core->modules[module].data[iterator] = 0;
    }
    core->modules[module].step = step;
    return zeno_true;
}

zeno_bool zeno_core_set(zeno_core  *core,
                        zeno_bool (*step)(zeno_core*)) {
    zeno_cell iterator;
    if(core == zeno_null) {
        return zeno_false;
    }
    for(iterator = 0;
        iterator < zeno_module_count;
        iterator++) {
        zeno_module_set(core, iterator, zeno_null);
    }
    for(iterator = 0;
        iterator < zeno_memory_size;
        iterator++) {
        core->memory[iterator] = 0;
    }
    for(iterator = 0;
        iterator < zeno_core_size;
        iterator++) {
        core->data[iterator] = 0;
    }
    core->step   = step;
    core->active = zeno_true;
    return zeno_true;
}
